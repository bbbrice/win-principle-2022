# 2.7注册表结构和注册表值类型

<https://docs.microsoft.com/en-us/windows/win32/api/winreg/ns-winreg-valenta>
<https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-value-types>
<https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-value-types>

## 1 结构

包含有关注册表值的信息。 列表中的RegQueryMultipleValues 函数使用此结构。

```cpp
 typedef struct value_entW {  
     LPWSTR    ve_valuename;
     DWORD     ve_valuelen;
     DWORD_PTR ve_valueptr;
     DWORD     ve_type;
 } VALENTW, *PVALENTW;
```

##### **ve_valuename**

要检索的值的名称。 一定要在调用 RegQueryMultipleValues 之前设置这个成员。

**ve_valuelen**

ve_valueptr 指向的数据的大小，以字节为单位。

**ve_valueptr**

指向值条目数据的指针。 这是一个指向lpValueBuf 缓冲区中返回值的数据由RegQueryMultipleValues。

**ve_type**

ve_valueptr 指向的数据类型。 对于列表的可能的类型，请参阅注册表值类型。

## 2 值类型

以下示例遍历 REG_MULTI_SZ 字符串。

```cpp
#include <windows.h>
#include <tchar.h>
#include <stdio.h>

void SampleSzz(PTSTR pszz)
{
   _tprintf(_TEXT("\tBegin multi-sz string\n"));
   while (*pszz) 
   {
      _tprintf(_TEXT("\t\t%s\n"), pszz);
      pszz = pszz + _tcslen(pszz) + 1;
   }
   _tprintf(_TEXT("\tEnd multi-sz\n"));
}

int __cdecl main(int argc, char **argv)
{
   // Because the compiler adds a \0 at the end of quoted strings, 
   // there are two \0 terminators at the end. 

   _tprintf(_TEXT("Conventional multi-sz string:\n"));  
   SampleSzz(_TEXT("String1\0String2\0String3\0LastString\0"));

   _tprintf(_TEXT("\nTest case with no strings:\n"));  
   SampleSzz(_TEXT(""));

   return 0;
}
```

# 2.8枚举注册表子项

<https://docs.microsoft.com/en-us/windows/win32/sysinfo/enumerating-registry-subkeys>

***

此实验示例使用 RegQueryInfoKey、RegEnumKeyEx、和 RegEnumValue 函数枚举的子项指定的键。 传递给每个函数的 hKey 参数是一个打开密钥的句柄。 必须在该功能之前打开此键打电话然后关闭。

```cpp
//QueryKey - 枚举键的子键及其关联值。
//hKey - 要枚举其子键和值的键。

#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

void QueryKey(HKEY hKey) 
{ 
    TCHAR    achKey[MAX_KEY_LENGTH];   // 子项名称的缓冲区
    DWORD    cbName;                   // 名称字符串的大小
    TCHAR    achClass[MAX_PATH] = TEXT("");  // 类名缓冲区
    DWORD    cchClassName = MAX_PATH;  //类字符串的大小
    DWORD    cSubKeys=0;               // 子键数 
    DWORD    cbMaxSubKey;              // 最长的子密钥大小 
    DWORD    cchMaxClass;              // 最长的类字符串
    DWORD    cValues;              // 键值的数量
    DWORD    cchMaxValue;          // 最长值名称
    DWORD    cbMaxValueData;       // 最长值数据
    DWORD    cbSecurityDescriptor; // 安全描述符的大小
    FILETIME ftLastWriteTime;      // 最后写入时间
 
    DWORD i, retCode; 
 
    TCHAR  achValue[MAX_VALUE_NAME]; 
    DWORD cchValue = MAX_VALUE_NAME; 
 
    // 获取类名和值计数。
    retCode = RegQueryInfoKey(
        hKey,                    // 钥匙柄
        achClass,                // 类名缓冲区
        &cchClassName,           // 类字符串的大小
        NULL,                    // 预订的
        &cSubKeys,               // 子键数 
        &cbMaxSubKey,            // 最长的子密钥大小 
        &cchMaxClass,            // 最长的类字符串
        &cValues,                // 此键的值数
        &cchMaxValue,            // 最长值名称 
        &cbMaxValueData,         // 最长值数据
        &cbSecurityDescriptor,   // 安全描述符
        &ftLastWriteTime);       // 最后写入时间
 
    // 枚举子键，直到 RegEnumKeyEx 失败.
    
    if (cSubKeys)
    {
        printf( "\nNumber of subkeys: %d\n", cSubKeys);

        for (i=0; i<cSubKeys; i++) 
        { 
            cbName = MAX_KEY_LENGTH;
            retCode = RegEnumKeyEx(hKey, i,
                     achKey, 
                     &cbName, 
                     NULL, 
                     NULL, 
                     NULL, 
                     &ftLastWriteTime); 
            if (retCode == ERROR_SUCCESS) 
            {
                _tprintf(TEXT("(%d) %s\n"), i+1, achKey);
            }
        }
    } 
 
    // 枚举键值。 

    if (cValues) 
    {
        printf( "\nNumber of values: %d\n", cValues);

        for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++) 
        { 
            cchValue = MAX_VALUE_NAME; 
            achValue[0] = '\0'; 
            retCode = RegEnumValue(hKey, i, 
                achValue, 
                &cchValue, 
                NULL, 
                NULL,
                NULL,
                NULL);
 
            if (retCode == ERROR_SUCCESS ) 
            { 
                _tprintf(TEXT("(%d) %s\n"), i+1, achValue); 
            } 
        }
    }
}

int __cdecl _tmain()
{
   HKEY hTestKey;

   if( RegOpenKeyEx( HKEY_CURRENT_USER,
        TEXT("SOFTWARE\\Microsoft"),
        0,
        KEY_READ,
        &hTestKey) == ERROR_SUCCESS
      )
   {
      QueryKey(hTestKey);
   }
   
   RegCloseKey(hTestKey);
}
```