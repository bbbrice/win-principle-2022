# 1、查找并阅读MVVM有关思想
## 简述
MVVM是Model-View-ViewModel的简写。它本质上就是MVC 的改进版。MVVM 就是将其中的View 的状态和行为抽象化，让我们将视图 UI 和业务逻辑分开。当然这些事 ViewModel 已经帮我们做了，它可以取出 Model 的数据同时帮忙处理 View 中由于需要展示内容而涉及的业务逻辑。微软的WPF带来了新的技术体验，如Silverlight、音频、视频、3D、动画……，这导致了软件UI层更加细节化、可定制化。同时，在技术层面，WPF也带来了 诸如Binding、Dependency Property、Routed Events、Command、DataTemplate、ControlTemplate等新特性。MVVM（Model-View-ViewModel）框架的由来便是MVP（Model-View-Presenter）模式与WPF结合的应用方式时发展演变过来的一种新型架构框架。它立足于原有MVP框架并且把WPF的新特性糅合进去，以应对客户日益复杂的需求变化。
## 1.1、MVVM的定义
MVVM是Model-View-ViewModel（模型-视图-视图模型）的缩写形式。  
View就是用xaml实现的界面，负责与用户交互，接收用户输入，把数据展现给用户。  
ViewModel是一个C#类，负责收集需要绑定的数据和命令，聚合Model对象，通过View类的DataContext属性绑定到View，同时也可以处理一些UI逻辑。  
Model，就是系统中的对象，可包含属性和行为。  
三者之间的关系：View对应一个ViewModel，ViewModel可以聚合N个Model，ViewModel可以对应多个View  
这种模式的引入就是使用ViewModel来降低View和Model的耦合，说是降低View和Model的耦合。也可以说是是降低界面和逻辑的耦合，理想情况下界面和逻辑是完全分离的，单方面更改界面时不需要对逻辑代码改动，同样的逻辑代码更改时也不需要更改界面。同一个ViewModel可以使用完全不用的View进行展示，同一个View也可以使用不同的ViewModel以提供不同的操作。

## 1.2、MVVM的优势
1）低耦合：视图（View）可以独立于Model变化和修改，一个ViewModel可以绑定到不同的"View"上，当View变化的时候Model可以不变，当Model变化的时候View也可以不变；  
2）可重用性：你可以把一些视图逻辑放在一个ViewModel里面，让很多view重用这段视图逻辑；  
3）可独立开发：开发人员可以专注于业务逻辑和数据的开发（ViewModel），设计人员可以专注于页面设计，使用Expression Blend可以很容易设计界面并生成xml代码；  
4）可测试：界面素来是比较难于测试的，而现在测试可以针对ViewModel来写。

# 2、熟悉WPF窗体应用程序创建流程
项目如图：  

![image-20221212152617637](image/image-20221212152617637.png)



## 2.1、button
按钮控件，该按钮可以对 Click 事件做出反应。  
WPF 中的按钮都是从 ButtonBase 抽象类中派生出来。因此，ButtonBase的属性，button也具有。例如：Name，Background，Foreground，Foreground，Content，MaxHeight，MaxHeight等。  
此外，按钮具有许多事件，包括：

- Click 在单击 Button 时发生
- MouseDoubleClick 在双击或多次单击鼠标按钮时发生
- MouseDown 在鼠标位于此元素上并且按下任意鼠标按钮时发生
## 2.2、label
label表示控件的文本标签，类似于TextBlock。但Label使用的是Content属性而不是Text属性，这是因为Label内部可以放置任意类型的控件而不仅仅是字符串，同时内容也可以是一个字符串。
## 2.3、textBox
文本输入框控件。它允许最终用户在一行、对话输入、或多行编写。其常用属性有：Background，HorizontalAlignment，FontFamily，FontSize，Text，IsReadOnly（对用户而言是否只读），MaxLines/MinLines（最大可见行数/最小行数）等。
## 2.4、listbox
listbox是一个 ItemsControl，继承自ItemsControl下的Selector。它可以包含任何类型的对象的集合，可以是嵌套单选框控件，也可以由自定义类的对象组成。 并且一个 ListBox 中的多个项是可见的。
## 2.5、DataGrid
DataGrid用于在可自定义的网格中显示数据的控件。DataGrid 控件可用于显示单个表或显示一组表之间的分层关系。
## 2.6、combox
表示带有下拉列表的选择控件，通过单击控件上的箭头可显示或隐藏下拉列表。也叫下拉列表控件。
# 3、Lab_1实验准备  
## 3.1、系统为Windows10 专业版

![image-20221212152737094](image/image-20221212153004814.png)



## 3.2、申请GitHub（Gitee）仓库  

![image-20221212153004814](image/image-220221212153004814.png)



## 3.3、申请ssh密钥

![image-20221212153259880](image/image-20221212153259880.png)



